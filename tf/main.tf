
provider "aws" {
  region = "ap-southeast-1"
  profile = "localterraform"
}

// deploy lambda function
resource "aws_lambda_function" "go1_dataset" {
  function_name = "go1-dataset"
  filename = "dist/app.zip"
  handler = "app"
  source_code_hash = "${base64sha256(filebase64("dist/app.zip"))}"
  role = "${aws_iam_role.go1_dataset.arn}"
  runtime = "go1.x"
  memory_size = 128
  timeout = 1
}

resource "aws_iam_role" "go1_dataset" {
  name = "go1_dataset"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": {
    "Action": ["sts:AssumeRole"],
    "Principal": {"Service": "lambda.amazonaws.com"},
    "Effect": "Allow"
  }
}
POLICY
}

// attach a policy for cloudwatch logs write permission to go1_dataset role
resource "aws_iam_role_policy" "go1_dataset_cloudwatch_logs_policy" {
  name = "go1_dataset_cloudwatch_logs_policy"
  role = "${aws_iam_role.go1_dataset.id}"
  policy = "${data.aws_iam_policy_document.go1_dataset_cloudwatch_logs_policy.json}"
}

data "aws_iam_policy_document" "go1_dataset_cloudwatch_logs_policy" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:CreateLogGroup",
      "logs:PutLogEvents"
    ]

    resources = ["*"]
  }
}

// allow lambda function access s3 buckets
data "aws_iam_policy_document" "go1_dataset_s3_access_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]

    resources = ["arn:aws:s3:::example-*/*"]
  }
}

resource "aws_iam_role_policy" "go1_dataset_s3_access_policy" {
  name = "go1_dataset_s3_access_policy"
  role = "${aws_iam_role.go1_dataset.id}"
  policy = "${data.aws_iam_policy_document.go1_dataset_s3_access_policy.json}"
}


// create test bucket, allow lambda function execution permission
resource "aws_s3_bucket" "go1_dataset" {
  bucket = "example-go1-dataset"
  force_destroy = true
}
resource "aws_lambda_permission" "allow_bucket_execute" {
  statement_id = "AllowExecutionFromS3Bucket"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.go1_dataset.arn}"
  principal = "s3.amazonaws.com"
  source_arn = "${aws_s3_bucket.go1_dataset.arn}"
}

// trigger event s3:ObjectCreated to go1_dataset function
resource "aws_s3_bucket_notification" "go1_dataset_bucket_notification" {
  bucket = "${aws_s3_bucket.go1_dataset.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.go1_dataset.arn}"
    events = ["s3:ObjectCreated:*"]
    filter_prefix = "csv/"
    filter_suffix = ".csv"
  }
}
